# Willkommen im Repo für Flat File CMS
## Was gibt es hier zu sichten?

In diesem Repo und ich habe es nach mehreren Stunden geschafft, die readme.md zu erstellen, musste mich durch etliche englische Tutorien durchkämpfen. Wie man sehen kann, hat es nun funktioniert und ich kann auch im eingeloggten Zustand das Overview sehen. 

In diesem Repo werde ich euch mein Wissen zu den Flat File CMS präsentieren und ich möchte mich mit euch im Issue Tracker gerne austauschen. Meine Themen-Gebiete sind Flat File CMS, ein bisschen Static Page Generatoren, OpenSource CMS, Foren, Wikis und das Blogging und zu diesen Themen-Gebieten werde ich auch hier im Bitbucket-Wiki posten.

Aber eins vorweg und ich bin kein CMS-Entwickler, sondern ein Endanwender und sehe das alles aus Anwendersicht. Auf meinen Stammblog [Internetblogger.de](http://internetblogger.de) tauchen stets neue CMS-Erfahrungsberichte auf und durch dieses Bitbucket-Repo möchte ich auf mich und meine Projekte etwas mehr aufmerksam machen. 

Mal sehen, was ich damit erreichen kann. Wenn die readme.md erstmals erstellt ist, gibt es wahrlich kein zurück mehr oder :smile_cat: und dann kann man nur weiter machen und sich der Materie CMS widmen. 

## Auf GitHub
Ich habe noch ein GitHub-Repo hinter [AlexL777](https://github.com/AlexL777) und dort habe ich eine Zusammenstellung aus den Flat File CMS, die hier anders formuliert, vorkommen wird.

In dem Sinne wünsche ich euch hier viel Spass beim Stöbern und besucht mich mal wieder.

## Zu Flat File CMS Systemen
### Automad CMS
Dieses CMS ist auf http://automad.org daheim und ich habe es hinter [diesem Link](http://wpzweinull.ch/cms/automad) aufgesetzt. Das CMS schaut bei mir im Frontend dermassen aus.

![automad-cms-frontend.jpg](https://bitbucket.org/repo/5zqR46/images/1425753931-automad-cms-frontend.jpg)

Das Automad CMS kommt ohne den Composer daher und lässt sich relativ leicht installieren. Wie man es anstellen kann, könnt ihr meiner höher verlinkten Automad Webseite entnehmen. Bei diesem CMS kann man auch Disqus-Kommentare ermöglichen und das Automad CMS ist ein passabler Static Page Generator, den man bekommt.

Damit könnt ihr eine statische Webseite mit weiteren Seiten erstellen und Unterseiten kommen beim nächsten Release hinzu, so wurde mir auf GitHub gesagt. Ansonsten ist die Handhabung dieses Flat File CMS eher leicht als alles andere und man wird gut zurecht kommen können.

### Parvula CMS

Das Parvula CMS ist ein weiterer Static Page Generator und zudem auch noch ein Flat File CMS. Zu Parvula gibt es auch eine offizielle Webseite hinter [diesem Link](http://bafs.github.io/parvula/) und noch das [GitHub-Repo](https://github.com/BafS/parvula). Dieses CMS setze ich auf Wpzweinull.ch [im Unterverzeichnis](http://wpzweinull.ch/cms/parvula) ein. 

Das Parvula CMS schaut im Frontend so aus.

![parvula-cms-webseite-frontend.jpg](https://bitbucket.org/repo/5zqR46/images/98589419-parvula-cms-webseite-frontend.jpg)

Das sieht ganz gut aus und damit lässt sich gut arbeiten. Bei diesem CMS gibt es unter /admin ein Backend und wie man Parvula CMS installieren kann, entnehmt bitte diesen [Hinweisen](http://wpzweinull.ch/cms/parvula/parvula_installation). Die Installation erfolgt mit dem Composer, welchen man vorher installieren muss. Dann installiert man das Parvula CMS. 

Oft ist es so, dass wenn man ein Flat File CMS mit der Kommandozeile installiert, auch ein Unterverzeichnis genommen wird. Danach muss man die Installation in euer Wunschverzeichnis bzw. das Rootverzeichnis verschieben. Das könnt ihr bei eurem Webhoster im Account tun. Das geht auch sehr leicht.
